import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.BasePage;
import pages.FirstPage;

import static helpers.testData.*;

import static pages.BasePage.makeScreenOnTestFail;
import static pages.FirstPage.*;

public class firstExamplePOAndData {

    public WebDriver driver;
    public pages.BasePage BasePage;
    private FirstPage newFirstPage;

    @BeforeMethod
    public void setupClass() {
        BasePage = new BasePage();
        driver = BasePage.initialize_driver();
        newFirstPage = new FirstPage(driver);
    }


    @Test(description = "Тест поиска гугла с пейдж обджектом и выносом переменных", priority = 0)
    public void testOpenGoogleAndCheckWithPO() {

        //браузер на весь экран
        maximizeBrowser(driver);

        //Открываем страницу Гугла
        openAnyPage(driver, url);

        //Искали поле ввода формы поиска - а теперь не ищем:)
        //enterField(searchFieldGoogle, searchRequest);

        //жмем энтер
        pressEnterForElement(searchFieldGoogle);

        //ассерт 1
        checkElementDisplayed(searchResultsDiv);

        //ассерт 2
        checkElementDisplayed(searchResultWiki);


    }

    @AfterMethod
    public void teardown(ITestResult result) {
        makeScreenOnTestFail(result);
        if (driver != null) {
            driver.quit();
        }
    }
}
