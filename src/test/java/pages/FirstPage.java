package pages;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Step;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class FirstPage {

    public WebDriver driver;

    @FindBy(name = "q")
    public static WebElement searchFieldGoogle;

    @FindBy(id = "search")
    public static WebElement searchResultsDiv;

    @FindBy(xpath = "//*[contains(text(),'Selenium — Википедия')]")
    public static WebElement searchResultWiki;



    //initiate page with it's elements
    public FirstPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    //открытие поисковой страницы, может использоваться в любом тесте
    @Step("Открываем страницу гугла")
    public static void openGoogleSearchPage(WebDriver driver) {

        driver.get("http://www.google.com");

    }

    //открытие поисковой страницы, может использоваться в любом тесте
    @Step("Открываем веб страницу")
    public static void openAnyPage(WebDriver driver, String url) {

        driver.get(url);

    }


    public static void maximizeBrowser(WebDriver driver) {
        driver.manage().window().maximize();
    }

    @Step("Нажимаем клавишу Enter в переданном элементе")
    public static void pressEnterForElement (WebElement element) {

        element.sendKeys(Keys.ENTER);
    }

    @Step("Проверяем отображается ли элемент")
    public static void checkElementDisplayed(WebElement element) {

        Assert.assertTrue(element.isDisplayed());
    }

    @Step("Вводим в поле переданное значение")
    public static void enterField(WebElement element, String value) {
        element.sendKeys(value);
    }
}
