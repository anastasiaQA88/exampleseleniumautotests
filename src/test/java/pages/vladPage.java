package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class vladPage {

    public WebDriver driver;

    @FindBy(name = "login")
    public static WebElement loginField;

    @FindBy(name = "password")
    public static WebElement passwordField;

    @FindBy(xpath = "//button[contains(.,'Вход')]")
    public static WebElement loginButton;

    @FindBy(id = "client-list-filters-registered-at")
    public static WebElement selectFilter;

    @FindBy(xpath = "//table[@id='client-list-table']/tbody/tr/td[2]/a")
    public static WebElement clientF;

    //initiate page with it's elements
    public vladPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
}
