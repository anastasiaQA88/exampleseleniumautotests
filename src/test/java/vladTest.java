import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.BasePage;
import pages.FirstPage;
import pages.vladPage;

import java.time.Duration;

import static pages.BasePage.*;
import static pages.FirstPage.enterField;
import static pages.FirstPage.openAnyPage;
import static pages.vladPage.*;

public class vladTest {

    public WebDriver driver;
    public pages.BasePage BasePage;
    private FirstPage newFirstPage;
    private vladPage newVladPage;

    @BeforeMethod
    public void setupClass() {
        BasePage = new BasePage();
        driver = BasePage.initialize_driver();
        newFirstPage = new FirstPage(driver);
        newVladPage = new vladPage(driver);
    }


    @Test
    public void testRobo() {

        openAnyPage(driver, "https://robocredit.hlp.systems/employee/login");
        waitElementToBeClickable(driver, loginField);
        enterField(loginField, "admin");
        enterField(passwordField, "admin1");
        clickButton(driver, loginButton);
        waitUntilUrlToBe(driver, "https://robocredit.hlp.systems/employee/home");
        openAnyPage(driver, "https://robocredit.hlp.systems/employee/client");
        waitElementToBeClickable(driver, selectFilter);
        chooseFromSelect(selectFilter, 1);
        waitElementToBeClickable(driver, clientF);
        Assert.assertTrue(clientF.getAttribute("innerHTML").contains("Орлова Елизавета Антоновна"));

    }
}
